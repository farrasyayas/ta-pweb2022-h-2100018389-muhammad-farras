<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Anime kesukaan</title>
</head>
<body>
    <form action="prosescombobox.php" method="post" name="input">
        <h2>Pilih anime kesukaan</h2>
        <select name="anime">
            <option value="Naruto">Naruto</option>
            <option value="Boruto">Boruto</option>
            <option value="Attack on Titan">Attack on Titan</option>
        </select>
        <input type="submit" name="Pilih" value="Pilih"/>
    </form>
</body>
</html>